/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biodatadiri;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author ROG Liwax
 */
import java.io.*;
class Utama 
{
    public static void main (String [] args) throws Exception
    {
        BufferedReader br = new BufferedReader (new InputStreamReader (System.in));
      //instance of class
        BiodataDiri DD = new BiodataDiri();
      //input
        System.out.print("Masukkan Nama : ");
        String nama = br.readLine();
        System.out.print("Masukkan NIS : ");
        String nis = br.readLine();
        System.out.print("Masukkan Tempat Lahir : ");
        String tmptlahir = br.readLine();
        System.out.print("Masukkan Tanggal Lahir : ");
        String tgllahir = br.readLine();
        System.out.print("Masukkan Jenis Kelamin : ");
        String jenkel = br.readLine();
        System.out.print("Masukkan Alamat di Malang : ");
        String alamat = br.readLine();
        System.out.print("Tuliskan Motto Hidup : ");
        String motto = br.readLine();
        DD.setNama(nama);
        DD.setNis(nis);
        DD.setTempatLahir(tmptlahir);
        DD.setTglLahir(tgllahir);
        DD.setJenkel(jenkel);
        DD.setAlamat(alamat);
        DD.setMotto(motto);
      //output
        System.out.println("=============");
        System.out.println("BIODATA SISWA");
        System.out.println("=============");
        System.out.println("Nama :"+ DD.getNama());
        System.out.println("NIS  :"+ DD.getNis());
        System.out.println("Tempat Lahir  :"+DD.getTempatLahir());
        System.out.println("Tanggal Lahir :"+DD.getTglLahir());
        System.out.println("Jenis Kelamin :"+DD.getJenkel());
        System.out.println("Alamat di Malang :"+DD.getAlamat());
        System.out.println("Motto Hidup :"+DD.getMotto());

    } 
}
