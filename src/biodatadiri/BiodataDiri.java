/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biodatadiri;

/**
 *
 * @author ROG Liwax
 */
class BiodataDiri 
{
//deklarasi
     private String nama, tgllahir, tmptlahir, alamat, jeniskelamin, motto, nis;
//setter
     public void setNama(String nama)
    {
        this.nama=nama; 
    }
    public void setTglLahir(String tgllahir)
    {
        this.tgllahir=tgllahir;
    }
     public void setTempatLahir(String tmptlahir)
    {
        this.tmptlahir=tmptlahir;
    }
      public void setNis(String nis)
    {
        this.nis=nis;
    }
     public void setAlamat(String alamat)
    {
        this.alamat=alamat; 
    } 
     public void setJenkel(String jeniskelamin)
    {
        this.jeniskelamin=jeniskelamin; 
    }
     public void setMotto(String motto)
    {
        this.motto=motto; 
    }
//getter
    public String getNama()
      {
          return nama;
      }
      public String getTglLahir()
      {
          return tgllahir;
      }
      public String getTempatLahir()
      {
          return tmptlahir;
      }
      public String getNis()
      {
          return nis;
      }
      public String getAlamat()
      {
          return alamat;
      }
      public String getJenkel()
      {
          return jeniskelamin;
      }
      public String getMotto()
      {
          return motto;
      }
}
